import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { TimeResponse } from './time-response';
import { Observable, Subject, Subscription, BehaviorSubject  } from 'rxjs';
import { shareReplay, switchMap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ReactiveServiceService {

  subject: Subject<string>;
  subjectSub: Subscription;
  getCurrentTime$: Observable<TimeResponse>;

  constructor(private http: HttpClient) {
    this.subject = new BehaviorSubject('utc');
   }

   public connectToTime(): Observable<TimeResponse> {

    if (!this.getCurrentTime$) {
      this.getCurrentTime$ = this.subject.asObservable()
      .pipe(
        switchMap(x => this.http.get<TimeResponse>(`http://worldclockapi.com/api/json/${x}/now`)),
        shareReplay(1)
      );
    }

    return this.getCurrentTime$;
   }

  public getCurrentTime(timezone: string) {
    this.subject.next(timezone);          
  }
}

