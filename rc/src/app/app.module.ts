import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { ToolOneComponent } from './toolbox/tool-one/tool-one.component';
import { ToolTwoComponent } from './toolbox/tool-two/tool-two.component';
import { ToolThreeComponent } from './toolbox/tool-three/tool-three.component';

@NgModule({
  declarations: [
    AppComponent,
    ToolOneComponent,
    ToolTwoComponent,
    ToolThreeComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
