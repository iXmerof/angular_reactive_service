import { Component, OnInit } from '@angular/core';
import { ReactiveServiceService } from 'src/app/reactive-service.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { TimeResponse } from 'src/app/time-response';

@Component({
  selector: 'app-tool-one',
  templateUrl: './tool-one.component.html',
  styleUrls: ['./tool-one.component.sass']
})
export class ToolOneComponent implements OnInit {

  time$: Observable<string>;
  timezone = 'utc';

  constructor(private rc: ReactiveServiceService) {
    this.time$ = this.rc.connectToTime()
    .pipe(
      map(x => x.timeZoneName)
    );
   }
  
  ngOnInit() {
  }

  onButtonClick() {
    this.timezone = this.timezone === 'utc' ? 'est' : 'utc';
    this.rc.getCurrentTime(this.timezone);
    
  }


}
