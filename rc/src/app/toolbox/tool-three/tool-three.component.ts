import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { TimeResponse } from 'src/app/time-response';
import { ReactiveServiceService } from 'src/app/reactive-service.service';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-tool-three',
  templateUrl: './tool-three.component.html',
  styleUrls: ['./tool-three.component.sass']
})
export class ToolThreeComponent implements OnInit {

  time$: Observable<string>;

  constructor(private rc: ReactiveServiceService) {
    this.time$ = rc.connectToTime()
    .pipe(
      map(x => x.timeZoneName)
    );
   }

  ngOnInit() {
  }

}
