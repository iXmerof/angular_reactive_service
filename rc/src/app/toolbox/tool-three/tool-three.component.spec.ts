import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ToolThreeComponent } from './tool-three.component';

describe('ToolThreeComponent', () => {
  let component: ToolThreeComponent;
  let fixture: ComponentFixture<ToolThreeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ToolThreeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ToolThreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
