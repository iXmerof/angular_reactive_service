import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ToolTwoComponent } from './tool-two.component';

describe('ToolTwoComponent', () => {
  let component: ToolTwoComponent;
  let fixture: ComponentFixture<ToolTwoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ToolTwoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ToolTwoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
