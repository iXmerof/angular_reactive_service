import { Component, OnInit } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { TimeResponse } from 'src/app/time-response';
import { ReactiveServiceService } from 'src/app/reactive-service.service';
import { FormControl } from '@angular/forms';
import { map, tap } from 'rxjs/operators';


@Component({
  selector: 'app-tool-two',
  templateUrl: './tool-two.component.html',
  styleUrls: ['./tool-two.component.sass']
})
export class ToolTwoComponent implements OnInit {

  time$: Observable<string>;
  formControl1: FormControl;
  formControlValueChanged: Subscription;
  timezone = 'utc';

  constructor(private rc: ReactiveServiceService) {
    this.formControl1 = new FormControl(this.timezone);
    
    this.time$ = this.rc.connectToTime()
    .pipe(
      map(x => x.timeZoneName)
    );

    this.formControlValueChanged = this.formControl1.valueChanges
    .pipe(
      tap(x => {
        this.timezone = this.timezone === 'utc' ? 'est' : 'utc';
        this.rc.getCurrentTime(this.timezone);
      })
    )
    .subscribe();
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.formControlValueChanged.unsubscribe();
  }

}
