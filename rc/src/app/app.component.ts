import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { TimeResponse } from './time-response';
import { ReactiveServiceService } from './reactive-service.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  time$: Observable<string>;

  constructor(private rc: ReactiveServiceService) {
    this.time$ = rc.connectToTime()
    .pipe(
      map(x => `${x.currentDateTime} in timezone ${x.timeZoneName}`)
    );
  }
}
