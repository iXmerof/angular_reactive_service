
export class TimeResponse {
    $id: number;
    currentDateTime: Date;
    utcOffset: string;
    isDayLightSavingsTime: boolean;
    dayOfTheWeek: string;
    timeZoneName: string;
    currentFileTime: number;
    ordinalDate: string;
    serviceResponse: object;
}